import React from 'react';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/PriceScreen';
import SettingsScreen from '../screens/SacScreen';

const HomeStack = createStackNavigator({
  Home: HomeScreen,
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={'home'}
    />
  ),
};

const priceStack = createStackNavigator({
  Links: LinksScreen,
});

priceStack.navigationOptions = {
  tabBarLabel: 'Price',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={'dollar'}
    />
  ),
};

const sacStack = createStackNavigator({
  Settings: SettingsScreen,
});

sacStack.navigationOptions = {
  tabBarLabel: 'Sac',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={'bar-chart-o'}
    />
  ),
};

export default createBottomTabNavigator({
  HomeStack,
  priceStack,
  sacStack,
});
