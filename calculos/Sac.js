
const calcularAmortizacao = (p0, n) => {
    return p0 / n;
}

round = (value) => {
    return Math.round(value * 100) / 100;
}

class Row {
    constructor(id, prestacao, saldo, amortizacao, juros) {
        this._id = id;
        this._prestacao = prestacao;
        this._saldo = saldo;
        this._amortizacao = amortizacao;
        this._juros = juros;
    }

    get id() {
        return this._id;
    }

    get prestacao() {
        return this._prestacao;
    }

    get saldo() {
        return this._saldo;
    }

    get amortizacao() {
        return this._amortizacao;
    }

    get juros() {
        return this._juros;
    }
}

export default class Sac {
    constructor(saldo, qtdPrestacoes, taxa) {
        this._saldo = saldo;
        this._qtdPrestacoes = qtdPrestacoes;
        this._taxa = taxa / 100;
        this.rows = []
        this._amortizacao = round(calcularAmortizacao(this._saldo, this._qtdPrestacoes));

        // Tempo 0
        this.rows.push(new Row(0, 0, this._saldo, 0, 0));
    }

    calcular() {
        for (let i = 1; i <= this._qtdPrestacoes; i++) {
            const juros = round(this.rows[i - 1].saldo * this._taxa);
            const saldo = round(this.rows[i - 1].saldo - this._amortizacao);
            const prestacao = round(juros + this._amortizacao);

            this.rows.push(new Row(i, prestacao, saldo, this._amortizacao, juros))
        }

        return this.rows;
    }
}
