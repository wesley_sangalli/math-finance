
const calcularPrestacao = (p0, n, i) => {
    return p0 * frc(i, n);
}

frc = (i, n) => {
    return (Math.pow((1 + i), n) * i) / (Math.pow((1 + i), n) - 1);
}

fva = (i, n) => {
    return (Math.pow((1 + i), n) - 1) / (Math.pow((1 + i), n) * i);
}

round = (value) => {
    return Math.round(value * 100) / 100;
}

class Row {
    constructor(id, prestacao, saldo, amortizacao, juros) {
        this._id = id;
        this._prestacao = prestacao;
        this._saldo = saldo;
        this._amortizacao = amortizacao;
        this._juros = juros;
    }

    get id() {
        return this._id;
    }

    get prestacao() {
        return this._prestacao;
    }

    get saldo() {
        return this._saldo;
    }

    get amortizacao() {
        return this._amortizacao;
    }

    get juros() {
        return this._juros;
    }
}

export default class Price {
    constructor(saldo, qtdPrestacoes, taxa) {
        this._saldo = saldo;
        this._qtdPrestacoes = qtdPrestacoes;
        this._taxa = taxa / 100;
        this._prestacao = round(calcularPrestacao(this._saldo, this._qtdPrestacoes, this._taxa));
        this.rows = []

        // Tempo 0
        this.rows.push(new Row(0, 0, this._saldo, 0, 0));
    }

    calcular() {
        for (let i = 1; i <= this._qtdPrestacoes; i++) {
            const juros = round(this.rows[i - 1].saldo * this._taxa);
            const amortizacao = round(this._prestacao - juros);
            const saldo = round(this.rows[i - 1].saldo - amortizacao);

            this.rows.push(new Row(i, this._prestacao, saldo, amortizacao, juros))
        }

        return this.rows;
    }
}

// const price = new Price(3000, 5, 9);
// const rows = price.calcular()
// console.log(rows);
// console.log(rows[1].saldo);