import React, { Component } from 'react';
import {
  Alert,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import accounting from 'accounting'
import commonStyles from '../commonStyles'
import Sac from "../calculos/Sac"
import { Button, Card, Slider } from 'react-native-elements';
import { TextInputMask } from 'react-native-masked-text'


export default class PriceScreen extends Component {
  static navigationOptions = {
    title: 'MÉTODO SAC',
    headerStyle: {
      backgroundColor: commonStyles.colors.default
    },
    headerTitleStyle: {
      color: 'white',
      textAlign: 'center',
      flexGrow: 1,
      alignSelf: 'center',
    },
  };

  constructor(props) {
    super(props)
    this.state = {
      showResults: false,
      rows: [],
      init: 0,
      end: 0,
    }
  }

  calc = async () => {
    if (!this.state.valor || !this.state.taxa || !this.state.parcelas) {
      Alert.alert('Dados inválidos', 'Todos os valores são obrigatórios');
      return
    }

    const sac = new Sac(this.state.valor, this.state.parcelas, this.state.taxa);
    const rows = await sac.calcular();

    this.setState({ rows });
    this.setState({ showResults: true });
  }

  formatMonetaryValue = (value) => {
    return accounting.formatMoney(parseFloat(value), 'R$', 2, ".", ",");
  }

  getPrestacaoForIndex = (index) => {
    let prestacao = !!this.state.rows[index] && this.state.rows[index].prestacao;
    return this.formatMonetaryValue(prestacao);
  }
  getSaldoForIndex = (index) => {
    let saldo = !!this.state.rows[index] && this.state.rows[index].saldo;
    if (index === this.state.rows.length - 1) {
      saldo = Math.round(saldo);
    }

    return this.formatMonetaryValue(saldo);
  }
  getJurosForIndex = (index) => {
    let juros = !!this.state.rows[index] && this.state.rows[index].juros;
    return this.formatMonetaryValue(juros);
  }

  getAmortizacaoForIndex = (index) => {
    let amortizacao = !!this.state.rows[index] && this.state.rows[index].amortizacao;
    return this.formatMonetaryValue(amortizacao);
  }

  getAmortizacaoAcumulada = (inicio, fim) => {
    const intervalo = this.state.rows.slice(inicio, fim + 1);
    let acumulado = 0;
    intervalo.forEach(row => acumulado += row.amortizacao);

    return this.formatMonetaryValue(acumulado);
  }

  getJurosAcumulado = (inicio, fim) => {
    const intervalo = this.state.rows.slice(inicio, fim + 1);
    let acumulado = 0;
    intervalo.forEach(row => acumulado += row.juros);

    return this.formatMonetaryValue(acumulado);
  }

  render() {
    return (
      <ScrollView style={styles.scrollContainer} contentContainerStyle={styles.container}>
        <View styles={styles.form}>
          <View style={styles.formItem}>
            <Text style={styles.label}>Valor do Financiamento</Text>
            <TextInputMask
              style={styles.input}
              placeholder="Valor R$"
              ref={ref => (this.fieldValor = ref)}
              type={'money'}
              value={this.state.valorFormatado}
              onChangeText={text => {
                this.setState({ valorFormatado: text })
                this.setState({ valor: this.fieldValor.getRawValue() })
              }}></TextInputMask>
          </View>
          <View style={styles.formItem}>
            <Text style={styles.label}>Numero de Parcelas</Text>
            <TextInputMask
              style={styles.input}
              placeholder="Quantidade Parcelas"
              type={'only-numbers'}
              onChangeText={text => !!text && this.setState({ parcelas: text })}
              value={this.state.parcelas}></TextInputMask>
          </View>
          <View style={styles.formItem}>
            <Text style={styles.label}>Taxa de Financiamento</Text>
            <TextInputMask
              style={styles.input}
              placeholder="Taxa %"
              type={'only-numbers'}
              onChangeText={text => !!text && this.setState({ taxa: text })}
              value={this.state.taxa}></TextInputMask>
          </View>
        </View>
        <View>
          <Button
            title='Calcular'
            onPress={this.calc}
            buttonStyle={styles.button}
            rounded
          />
        </View>
        {this.state.showResults &&
          <View style={{ flex: 1, alignItems: 'stretch', padding: 10 }}>
            <View>
              <Text>
                <Text>Inicio na </Text>
                <Text style={{ fontWeight: 'bold' }}>{this.state.init}ª </Text>
                <Text>Parcela</Text>
              </Text>
              <Slider
                step={1}
                maximumValue={this.state.end}
                onValueChange={(value) => this.setState({ init: value })}
                value={this.state.init}
                thumbTintColor={commonStyles.colors.default}
              />

              <Text>
                <Text>Fim na </Text>
                <Text style={{ fontWeight: 'bold' }}>{this.state.end}ª </Text>
                <Text>Parcela</Text>
              </Text>
              <Slider
                step={1}
                maximumValue={parseFloat(this.state.parcelas)}
                onValueChange={(value) => this.setState({ end: value })}
                value={this.state.end}
                thumbTintColor={commonStyles.colors.default}
              />
            </View>

            <Card title='Valor das Prestações' titleStyle={styles.cardTitle}>
              <Text style={styles.cardText}>{this.getPrestacaoForIndex(this.state.end)} </Text>
            </Card>
            <Card title='Saldo Devedor' titleStyle={styles.cardTitle}>
              <Text style={styles.cardText}>{this.getSaldoForIndex(this.state.end)} </Text>
            </Card>
            <Card title='Parcela de Juros' titleStyle={styles.cardTitle}>
              <Text style={styles.cardText}>{this.getJurosForIndex(this.state.end)} </Text>
            </Card>
            <Card title='Parcela de Amortização' titleStyle={styles.cardTitle}>
              <Text style={styles.cardText}>{this.getAmortizacaoForIndex(this.state.end)} </Text>
            </Card>
            <Card title='Amortização Acumulada' titleStyle={styles.cardTitle}>
              <Text style={styles.cardText}>{this.getAmortizacaoAcumulada(this.state.init, this.state.end)} </Text>
            </Card>
            <Card title='Juros Acumulado' titleStyle={styles.cardTitle}>
              <Text style={styles.cardText}> {this.getJurosAcumulado(this.state.init, this.state.end)} </Text>
            </Card>
          </View>
        }

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  scrollContainer: {
    flex: 1,
    backgroundColor: 'white',
  },
  container: {
    justifyContent: 'space-between'
  },
  form: {
    justifyContent: 'flex-start'
  },
  formItem: {
    flex: 1,
    flexDirection: 'column',
  },
  button: {
    margin: 20,
    marginRight: 30,
    backgroundColor: commonStyles.colors.default,
  },
  label: {
    textAlign: 'center',
    alignSelf: 'stretch',
    fontSize: 15,
    padding: 10
  },
  input: {
    padding: 5,
    width: '80%',
    height: 40,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#e3e3e3',
    borderRadius: 6,
    marginLeft: 40,
    marginRight: 40
  },
  cardTitle: {
    backgroundColor: commonStyles.colors.default,
    color: 'white',
    fontSize: 15,
    fontWeight: 'bold',
    padding: 10
  },
  cardText: {
    textAlign: 'center',
    fontWeight: 'bold'
  }
});
