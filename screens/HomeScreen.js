import commonStyles from '../commonStyles'
import React from 'react';
import { Feather } from '@expo/vector-icons';
import * as Animatable from 'react-native-animatable';

import {
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.introContainer}>
          <Text style={styles.title}>FINANCE</Text>
          <Text style={styles.subtitle}>Calculadora de Financiamentos</Text>
        </View>
        <View style={styles.barInfoContainer}>
          <Text style={styles.barInfoText}>Escolha um método abaixo para simular o financiamento</Text>
        </View>
        {/*<View style={styles.animation}>
          <Animatable.Text animation="slideInDown" iterationCount={Infinity} direction="alternate">
            <Feather name="arrow-down" size={60} color="lightskyblue" />
          </Animatable.Text>
        </View>*/}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 100,
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },
  introContainer: {
    alignItems: 'center',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 60,
    color: commonStyles.colors.default
  },
  subtitle: {
    fontSize: 20,
    color: 'lightgray'
  },
  barInfoContainer: {
    flex: 1,
    justifyContent: 'center',
    bottom: 50,
    left: 0,
    right: 0,
    padding: 30,
  },
  barInfoText: {
    fontSize: 20,
    color: '#2e78b7',
    textAlign: 'center',
  },
  animation: {
    alignItems: 'center'
  }
});
